import React from "react";

const Book = (props) => {
  const { title, autor, photo } = props.book;
  return (
    <article className="book">
      <img src={photo} alt="livre" />
      <h1>{title}</h1>
      <h4>{autor}</h4>
    </article>
  );
};

export default Book;
