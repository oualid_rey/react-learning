export const books = [
  {
    id: 1,
    title: "je t'aime a la folie",
    autor: "Oualid Ben daoud",
    photo:
      "https://images-na.ssl-images-amazon.com/images/I/81eB%2B7%2BCkUL._AC_UL200_SR200,200_.jpg",
  },
  {
    id: 2,
    title: "Le retour",
    autor: "Nicolas Rey",
    photo:
      "https://images-na.ssl-images-amazon.com/images/I/91JRZQT3GML._AC_UL200_SR200,200_.jpg",
  },
  {
    id: 3,
    title: "The Little Old Lady Who Was Not Afraid of Anything",
    autor: "Linda d. williams",
    photo:
      "https://images-na.ssl-images-amazon.com/images/I/816VzjZVGrL._AC_UL200_SR200,200_.jpg",
  },
];
