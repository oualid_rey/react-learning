import React from "react";
import ReactDom from "react-dom";
import "./index.css";
import { books } from "./books.jsx";
import Book from "./Book.jsx";

function BookList() {
  return (
    <section className="booklist">
      {books.map((book) => {
        // const { title, autor, photo } = book;
        return <Book key={book.id} book={book}></Book>;
      })}
    </section>
  );
}

ReactDom.render(<BookList />, document.getElementById("root"));
